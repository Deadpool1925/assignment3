//
// Created by Prabhsimran Singh on 2019-07-25.
//

#include <stdlib.h>
#include <stdio.h>

int ARRAY_SIZE = 5;

void printArray(int array[]) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%d ", array[i]);
    }
}

int main() {
    // Creating an unsorted array
    int array[] = {19, 25, 12, 24, 07};

    int count = 0;

    // printing that array
    printf("Unsorted array is: \n");
    printArray(array);


    // implementing the bubble sort
    for (int i = 0; i < ARRAY_SIZE; i++) {
        for (int j = 0; j < ARRAY_SIZE-1; j++) {
            int curr = array[j];
            int next = array[j + 1];

            if (curr < next) {
                int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }

            count = count + 1;
        }

    }

    // printing the sorted array
    printf("\nSorted array is: \n");
    printArray(array);

    printf("\nTotal loops are: %d\n", count);

    return 0;
}


