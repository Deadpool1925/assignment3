//
// Created by Prabhsimran Singh on 2019-07-25.
//

#include <stdlib.h>
#include <stdio.h>

int ARRAY_SIZE = 5;

void printArray(int arr[]) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%d ", arr[i]);
    }
}

int main() {

    // Unsorted array
    int array[] = {19, 25, 12, 24, 07};
    printf("Array \n");
    printArray(array);

    // new empty array to sort results
    int empArray[] = {0,0,0,0,0};

    for (int i = 0; i < ARRAY_SIZE; i++) {

        int largestValue = array[0];
        int positionOfLargest = 0;
        for (int j = 0; j < ARRAY_SIZE; j++) {
            int current = array[j];
            if (current > largestValue) {
                largestValue = current;
                positionOfLargest = j;
            }
        }
        printf("\n Largest item: %d\n", largestValue);

        empArray[i] = largestValue;

        array[positionOfLargest] = -999999;

    }

    printf("\n Array after Faking the delete: \n");
    printArray(array);

    printf("\n \n empty array after sorting: \n");
    printArray(empArray);
    return 0;
}



